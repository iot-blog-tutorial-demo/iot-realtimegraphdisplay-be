package com.godreg;

/*
 * Copyright 2012-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.List;
//import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.amazonaws.services.kinesis.clientlibrary.exceptions.InvalidStateException;
import com.amazonaws.services.kinesis.clientlibrary.exceptions.ShutdownException;
import com.amazonaws.services.kinesis.clientlibrary.exceptions.ThrottlingException;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorCheckpointer;
import com.amazonaws.services.kinesis.model.Record;
//import com.mongodb.DB;
//import com.mongodb.DBCollection;
//import com.mongodb.DBObject;
//import com.mongodb.Mongo;
//import com.mongodb.util.JSON;

import software.amazon.kinesis.lifecycle.ShutdownReason;

import redis.clients.jedis.Jedis;

/**
 * Processes records and checkpoints progress.
 */
public class AmazonKinesisApplicationSampleRecordProcessor implements IRecordProcessor {

//	private int i = 0;
	private static final Log LOG = LogFactory.getLog(AmazonKinesisApplicationSampleRecordProcessor.class);
	private String kinesisShardId;

	// Backoff and retry settings
	private static final long BACKOFF_TIME_IN_MILLIS = 3000L;
	private static final int NUM_RETRIES = 10;

	// Checkpoint about once a minute
	private static final long CHECKPOINT_INTERVAL_MILLIS = 60000L;
	private long nextCheckpointTimeInMillis;

	private final CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();

//    Mongo mongo = new Mongo("localhost", 27017);
//	DB db = mongo.getDB("iot");
//	DBCollection collection = db.getCollection("Device_Dynamic_Attributes");

	/**
	 * {@inheritDoc}
	 */
	public void initialize(String shardId) {
		LOG.info("Initializing record processor for shard: " + shardId);
		this.kinesisShardId = shardId;
	}

	/**
	 * {@inheritDoc}
	 */
	public void processRecords(List<Record> records, IRecordProcessorCheckpointer checkpointer) {
		LOG.info("Processing " + records.size() + " records from " + kinesisShardId);

		// Process records and perform all exception handling.
		processRecordsWithRetries(records);

		// Checkpoint once every checkpoint interval.
		if (System.currentTimeMillis() > nextCheckpointTimeInMillis) {
			checkpoint(checkpointer);
			nextCheckpointTimeInMillis = System.currentTimeMillis() + CHECKPOINT_INTERVAL_MILLIS;
		}
	}

	/**
	 * Process records performing retries as needed. Skip "poison pill" records.
	 * 
	 * @param records Data records to be processed.
	 */
	private void processRecordsWithRetries(List<Record> records) {
		for (Record record : records) {
			boolean processedSuccessfully = false;
			for (int i = 0; i < NUM_RETRIES; i++) {
				try {
					//
					// Logic to process record goes here.
					//
					// System.out.println(record);
					processSingleRecord(record);

					processedSuccessfully = true;
					break;
				} catch (Throwable t) {
					LOG.warn("Caught throwable while processing record " + record, t);
				}

				// backoff if we encounter an exception.
				try {
					Thread.sleep(BACKOFF_TIME_IN_MILLIS);
				} catch (InterruptedException e) {
					LOG.debug("Interrupted sleep", e);
				}
			}

			if (!processedSuccessfully) {
				LOG.error("Couldn't process record " + record + ". Skipping the record.");
			}
		}
	}

	/**
	 * Process a single record.
	 * 
	 * @param record The record to be processed.
	 */
	private void processSingleRecord(Record record) {
		// TODO Add your own record processing logic here

		Jedis jedis = new Jedis("localhost");
		String channel = "godreg-channel";
		//System.out.println("Starting publisher for channel " + channel);

		String data = null;
		try {
			// For this app, we interpret the payload as UTF-8 chars.
			data = decoder.decode(record.getData()).toString();
			System.out.println(data);

			
			  
			  try {
			  
//			  URL url = new URL("http://127.0.0.1:9000/api"); HttpURLConnection conn =
//			  (HttpURLConnection) url.openConnection(); conn.setDoOutput(true);
//			  conn.setRequestMethod("GET"); conn.setRequestProperty("Content-Type",
//			  "application/json");
//			  
//			  
//			  
//			  OutputStream os = conn.getOutputStream(); //os.write(input.getBytes());
//			  os.write(data.getBytes()); os.flush();
//			  
//			  
//			  
//			  BufferedReader br = new BufferedReader(new InputStreamReader(
//			  (conn.getInputStream())));
//			  
//			  String output; 
//			  System.out.println("Output from Server .... \n");
//			  
//			  String outputFinal = "";
//			  while((output = br.readLine()) != null) 
//			  { 
//			  outputFinal=outputFinal+output;
//			  }
			  jedis.publish(channel,data);
			  
			 
//			  System.out.println(outputFinal);
//			 
//			  
//			  DdbPush obj = new DdbPush();
//			  try {
//			  obj.sendData(outputFinal);
//			  }
//			  catch (Exception e)
//			  {e.printStackTrace();}
//			 
//			  
//			  conn.disconnect();
//			  
			  } catch (Exception e) {
			  
			  //e.printStackTrace();
				  LOG.error("Malformed data: " + e);
			  }
			  

			  
			 

		} catch (NumberFormatException e) {
			LOG.info("Record does not match sample record format. Ignoring record with data; " + data);
		} catch (CharacterCodingException e) {
			LOG.error("Malformed data: " + data, e);
		}
		
		  finally {
			    jedis.close();
			  }
		
		
	}

	/**
	 * {@inheritDoc}
	 */
	public void shutdown(IRecordProcessorCheckpointer checkpointer, ShutdownReason reason) {
		LOG.info("Shutting down record processor for shard: " + kinesisShardId);
		// Important to checkpoint after reaching end of shard, so we can start
		// processing data from child shards.
		if (reason == ShutdownReason.LEASE_LOST) {
			checkpoint(checkpointer);
		}
	}

	/**
	 * Checkpoint with retries.
	 * 
	 * @param checkpointer
	 */
	private void checkpoint(IRecordProcessorCheckpointer checkpointer) {
		LOG.info("Checkpointing shard " + kinesisShardId);
		for (int i = 0; i < NUM_RETRIES; i++) {
			try {
				checkpointer.checkpoint();
				break;
			} catch (ShutdownException se) {
				// Ignore checkpoint if the processor instance has been shutdown (fail over).
				LOG.info("Caught shutdown exception, skipping checkpoint.", se);
				break;
			} catch (ThrottlingException e) {
				// Backoff and re-attempt checkpoint upon transient failures
				if (i >= (NUM_RETRIES - 1)) {
					LOG.error("Checkpoint failed after " + (i + 1) + "attempts.", e);
					break;
				} else {
					LOG.info("Transient issue when checkpointing - attempt " + (i + 1) + " of " + NUM_RETRIES, e);
				}
			} catch (InvalidStateException e) {
				// This indicates an issue with the DynamoDB table (check for table, provisioned
				// IOPS).
				LOG.error("Cannot save checkpoint to the DynamoDB table used by the Amazon Kinesis Client Library.", e);
				break;
			}
			try {
				Thread.sleep(BACKOFF_TIME_IN_MILLIS);
			} catch (InterruptedException e) {
				LOG.debug("Interrupted sleep", e);
			}
		}
	}

	public void shutdown(IRecordProcessorCheckpointer checkpointer,
			com.amazonaws.services.kinesis.clientlibrary.lib.worker.ShutdownReason reason) {
		// TODO Auto-generated method stub

	}
}