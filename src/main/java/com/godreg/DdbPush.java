package com.godreg;


import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;

//import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
//import org.json.simple.parser.ParseException;




public class DdbPush {
	  static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
	    static DynamoDB dynamoDB = new DynamoDB(client);

Calendar calendar = Calendar.getInstance(); 
//SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss:SSS");  
SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss.SSS'Z'");  
String d=formatter.format(calendar.getTime());







	    
	public void sendData(String data)  {
		try {
		
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(data);
        
        int pid = Integer.parseInt( jsonObject.get("Plant Id").toString());
        double pif = (Double) jsonObject.get("% Iron Feed");
        double pic = (Double) jsonObject.get("% Iron Concentrate");
        double psf = (Double) jsonObject.get("% Silica Feed");
        double psc = (Double) jsonObject.get("% Silica Concentrate ");
        double af = (Double) jsonObject.get("Amina Flow");
        double oph = (Double) jsonObject.get("Ore Pulp pH");
        double opd = (Double) jsonObject.get("Ore Pulp Density");
        double opf = (Double) jsonObject.get("Ore Pulp Flow");
        double sf = (Double) jsonObject.get("Starch Flow");
        
      
       
        double fca1 = (Double) jsonObject.get("Flotation Column 01 Air Flow");
        double fca2 = (Double) jsonObject.get("Flotation Column 02 Air Flow");
        double fca3 = (Double) jsonObject.get("Flotation Column 03 Air Flow");
        double fca4 = (Double) jsonObject.get("Flotation Column 04 Air Flow");
        double fca5 = (Double) jsonObject.get("Flotation Column 05 Air Flow");
        double fca6 = (Double) jsonObject.get("Flotation Column 06 Air Flow");
        double fca7 = (Double) jsonObject.get("Flotation Column 07 Air Flow");
        
        double fcl1 = (Double) jsonObject.get("Flotation Column 01 Level");
        double fcl2 = (Double) jsonObject.get("Flotation Column 02 Level");
        double fcl3 = (Double) jsonObject.get("Flotation Column 03 Level");
        double fcl4 = (Double) jsonObject.get("Flotation Column 04 Level");
        double fcl5 = (Double) jsonObject.get("Flotation Column 05 Level");
        double fcl6 = (Double) jsonObject.get("Flotation Column 06 Level");
        double fcl7 = (Double) jsonObject.get("Flotation Column 07 Level");
        
        
        
        
        Table table = dynamoDB.getTable("Aiml_KCL_Save_Data");
		
		Item item = new Item()
			    .withPrimaryKey("myDate", d)
			    .withNumber("plantId", pid)
			    .withNumber("iron_Feed", pif)
			    .withNumber("iron_Concentrate", pic)
			    .withNumber("selica_Feed", psf)
			    .withNumber("selica_Concentrate", psc)
			    .withNumber("amina_Flow", af)
			    .withNumber("flotation_Column_01_Air_Flow", fca1)
			    .withNumber("flotation_Column_01_Level", fcl1)
			    .withNumber("flotation_Column_02_Air_Flow", fca2)
			    .withNumber("flotation_Column_02_Level", fcl2)
			    .withNumber("flotation_Column_03_Level", fcl3)
			    .withNumber("flotation_Column_03_Air_Flow", fca3)
			    .withNumber("flotation_Column_04_Level", fcl4)
			    .withNumber("flotation_Column_04_Air_Flow", fca4)
			    .withNumber("flotation_Column_05_Air_Flow", fca5)
			    .withNumber("flotation_Column_05_Level", fcl5)
			    .withNumber("flotation_Column_06_Air_Flow", fca6)
			    .withNumber("flotation_Column_06_Level", fcl6)
			    .withNumber("flotation_Column_07_Air_Flow", fca7)
			    .withNumber("flotation_Column_07_Level", fcl7)
			    .withNumber("ore_pulp_Density", opd)
			    .withNumber("ore_pulp_PH", oph)
			    .withNumber("ore_Pulp_Flow", opf)
			    .withNumber("starch_Flow", sf);
       
        
        
//        	System.out.println(pif);
//        	System.out.println(pic);
//        	System.out.println(psf);
//        	System.out.println(psc);
//        	System.out.println(af);
//           	System.out.println(fca1);
//        	System.out.println(fca2);
//           	System.out.println(fca3);
//        	System.out.println(fca4);
//           	System.out.println(fca5);
//        	System.out.println(fca6);
//        	System.out.println(fca7);
//        	
//           	System.out.println(fcl1);
//        	System.out.println(fcl2);
//           	System.out.println(fcl3);
//        	System.out.println(fcl4);
//           	System.out.println(fcl5);
//        	System.out.println(fcl6);
//        	System.out.println(fcl7);
        	
        	
//        	System.out.println(formatter.format(calendar.getTime()));  
		
        //{"Amina Flow":564.697,"Flotation Column 03 Level":460.449,"Flotation Column 04 Level":439.92,"Ore Pulp Flow":396.533,"% Silica Feed":16.98,"Flotation Column 05 Air Flow":306.4,"Flotation Column 01 Air Flow":250.73,"Flotation Column 02 Level":443.269,"Flotation Column 04 Air Flow":295.096,"Flotation Column 07 Air Flow":251.873,"% Iron Feed":55.2,"Flotation Column 07 Level":425.458,"% Iron Concentrate":66.91,"Starch Flow":3079.1,"Flotation Column 01 Level":444.384,"Flotation Column 06 Level":433.539,"Ore Pulp Density":1.74,"Flotation Column 03 Air Flow":249.521,"% Silica Concentrate":1.2642823239997796,"Flotation Column 05 Level":451.588,"Ore Pulp pH":10.0705,"Flotation Column 06 Air Flow":250.356,"Flotation Column 02 Air Flow":248.90599999999998}
        
	
        //System.out.println(jsonObject.toJSONString());
		
		
		
		//push data in dynamo db
		//PutItemOutcome outcome = table.putItem(item);

		System.out.println("data inserted");
		
		
//		
//		ScanRequest scanRequest = new ScanRequest()
//				.withTableName("AIML_Alarm");
//				ScanResult result = client.scan(scanRequest);
//		
//		 try {
//
//     		for (Map<String, AttributeValue> item1 : result.getItems()){
//      	         //System.out.println(item1);
//      	         //System.out.println(item1.getClass().getName());
//      	         String parameter=(item1.get("Parameter")).getS().toString();
//      	        // System.out.println(parameter);
//
//      	         double min=Double.parseDouble((item1.get("min")).getN().toString());
//      	         System.out.println("\nMin Value: "+min);
//      	         
//      	         double max=Double.parseDouble((item1.get("max")).getN().toString());
//      	         System.out.println("Max Value: "+max);
//      	         
//      	         int plantId=Integer.parseInt((item1.get("plantId")).getN().toString());
//      	        // System.out.println(plantId);
//      	         
//      	         int flag=Integer.parseInt((item1.get("flag")).getN().toString());
//      	        // System.out.println(flag);
//      	         
//      	       double current = (Double) jsonObject.get(parameter);
//      	     System.out.println("Current "+parameter+" Value: "+current);
//      	       	
//      	       if (current < min)
//         	         System.out.println("current value is min\n");
//      	       	else if (current > max)
//      	       		System.out.println("current value is max\n");
//      	       	else
//         	       	 System.out.println("current value is normal\n");
//
//      	        
//   		}
//	    
//			
//			
//			 } catch (Exception e) {
//			    System.err.println("Cannot retrieve items.");
//			    System.err.println(e.getMessage());
//			 }
//				         
		
		
		
		
		
		
		
		}
		catch(Exception e) {
			e.printStackTrace();
			//System.out.println("json problem");
		}
	
	}
	

}
